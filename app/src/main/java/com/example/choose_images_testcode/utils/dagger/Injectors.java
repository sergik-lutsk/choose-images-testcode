package com.example.choose_images_testcode.utils.dagger;

import android.content.Context;

import com.example.choose_images_testcode.MyApp;
import com.example.choose_images_testcode.utils.dagger.app.AppComponent;
import com.example.choose_images_testcode.utils.dagger.app.AppModule;
import com.example.choose_images_testcode.utils.dagger.app.DaggerAppComponent;

import java.util.Map;
import java.util.WeakHashMap;


public final class Injectors {

    private static final Map<Context, AppComponent> sComponents = new WeakHashMap<>();

    private Injectors() {
    }

    public static AppComponent get(Context context) {
        context = context.getApplicationContext();
        AppComponent component = sComponents.get(context);
        if (component == null) {
            component = DaggerAppComponent.builder()
                    .application((MyApp) context.getApplicationContext())
                    .appModule(new AppModule())
                    .build();
            sComponents.put(context, component);
        }

        return component;
    }
}
