package com.example.choose_images_testcode.utils.dagger.app;

import com.example.choose_images_testcode.model.PreferenceRepository;
import com.example.choose_images_testcode.model.PreferenceRepositoryImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    @Singleton
    public PreferenceRepository provideSharedPrefsRepository() {
        return new PreferenceRepositoryImpl();
    }

//    @Provides
//    @Singleton
//    public NetworkRepository provideNetworkInteractor() {
//        return new NetworkRepositoryImpl();
//    }

//    @Provides
//    public Interactor.interactorAccount provideDeviceInteractor(PreferenceRepository prefsRepository, NetworkRepository networkRepository) {
//        return new AccountInteractor(prefsRepository, networkRepository);
//    }
}
