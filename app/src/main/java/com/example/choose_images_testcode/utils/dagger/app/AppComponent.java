package com.example.choose_images_testcode.utils.dagger.app;


import android.app.Application;

import com.example.choose_images_testcode.MyApp;
import com.example.choose_images_testcode.model.PreferenceRepositoryImpl;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import dagger.android.support.DaggerApplication;

@Component(modules = {
        AppModule.class,
        AndroidInjectionModule.class,
        AndroidSupportInjectionModule.class
})

@Singleton
public interface AppComponent extends AndroidInjector<DaggerApplication> {

    void inject(PreferenceRepositoryImpl preferenceRepository);

//    void inject(NetworkRepositoryImpl networkRepository);

    void inject(MyApp app);

    @Component.Builder
    interface Builder {

        @BindsInstance
        AppComponent.Builder application(Application application);

        @BindsInstance
        Builder appModule(AppModule appModule);

        AppComponent build();
    }
}
