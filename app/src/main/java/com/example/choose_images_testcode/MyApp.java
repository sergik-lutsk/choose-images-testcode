package com.example.choose_images_testcode;

import android.content.Context;

import com.example.choose_images_testcode.utils.dagger.app.AppComponent;
import com.example.choose_images_testcode.utils.dagger.app.AppModule;
import com.example.choose_images_testcode.utils.dagger.app.DaggerAppComponent;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public class MyApp extends DaggerApplication {

    private static Context context;

    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        //MultiDex.install(this);
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent appComponent = DaggerAppComponent.builder()
                .application(this)
                .appModule(new AppModule())
                .build();
        appComponent.inject(this);
        return appComponent;
    }
}
