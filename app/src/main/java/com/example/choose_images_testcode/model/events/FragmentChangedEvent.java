package com.example.choose_images_testcode.model.events;

import com.example.choose_images_testcode.ui.utils.fragments.FragmentsAnimationId;
import com.example.choose_images_testcode.ui.utils.fragments.FragmentsId;

public interface FragmentChangedEvent {

    void changeFragment(FragmentsId fragmentsId, FragmentsAnimationId fragmentsAnimationId);

    void setFragment(FragmentsId fragmentsId);
}
