package com.example.choose_images_testcode.model;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.choose_images_testcode.MyApp;
import com.example.choose_images_testcode.utils.LogUtils;

import static android.content.Context.MODE_PRIVATE;

public class PreferenceRepositoryImpl implements PreferenceRepository {

    public static final String API_KEY = "API_KEY";
    public static final String ACCOUNT_NUMBER = "ACCOUNT_NUMBER";
    public static final String PAYMENT_TYPES_ID = "PAYMENT_TYPES_ID";
    public static final String VERIFY_CODE = "VERIFY_CODE";
    private static final String PREF = "DB_NAME";

    private static final String URLS_ID = "URLS_ID";
    private SharedPreferences mPref;

    public PreferenceRepositoryImpl() {
        mPref = MyApp.getContext().getSharedPreferences(PREF, MODE_PRIVATE);
    }

    public PreferenceRepositoryImpl(Context context) {
        mPref = context.getSharedPreferences(PREF, MODE_PRIVATE);
    }

    @Override
    public void removeSimple(String key) {

        try {
            mPref.edit().remove(key).apply();
        } catch (Exception e) {
            LogUtils.logError(getClass().getName(), e);
        }
    }

    @Override
    public void setValueSimple(String key, Object value) {

        try {
            SharedPreferences.Editor editor = mPref.edit();
            if (value instanceof String) {
                String val = (String) value;
                editor.putString(key, val);
            }
            if (value instanceof Integer) {
                Integer val = (Integer) value;
                editor.putInt(key, val);
            }
            if (value instanceof Boolean) {
                Boolean val = (Boolean) value;
                editor.putBoolean(key, val);
            }
            editor.apply();
        } catch (Exception e) {
            LogUtils.logError(getClass().getName(), e);
        }
    }

    @Override
    public SharedPreferences getValueSimple() {
        return mPref;
    }

    @Override
    public boolean isValueExistSimple(String key) {

        try {
            return mPref.contains(key);
        } catch (Exception e) {
            LogUtils.logError(getClass().getName(), e);
            return false;
        }
    }
}