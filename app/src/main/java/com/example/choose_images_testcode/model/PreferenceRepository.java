package com.example.choose_images_testcode.model;

import android.content.SharedPreferences;

public interface PreferenceRepository {

    void setValueSimple(String key, Object value);

    void removeSimple(String key);

    boolean isValueExistSimple(String key);

    SharedPreferences getValueSimple();
}
