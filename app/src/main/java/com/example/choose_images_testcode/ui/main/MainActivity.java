package com.example.choose_images_testcode.ui.main;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.example.choose_images_testcode.R;
import com.example.choose_images_testcode.ui.buttons.ButtonsListFragmentTest;

public class MainActivity extends AppCompatActivity {


    private static final int FRAGMENTS_CONTEINER = R.id.containerFragments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButtonsListFragmentTest buttonsListFragment = new ButtonsListFragmentTest();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(FRAGMENTS_CONTEINER, buttonsListFragment, buttonsListFragment.getClass().getSimpleName()).commit();
    }
}
