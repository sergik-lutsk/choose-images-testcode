package com.example.choose_images_testcode.ui.buttons;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.choose_images_testcode.R;
import com.example.choose_images_testcode.ui.base.BaseFragment;
import com.example.choose_images_testcode.utils.ToastUtils;

import java.util.List;

import static android.app.Activity.RESULT_OK;

public class ButtonsListFragmentTest extends BaseFragment {

    private final int FILE_PICKER_REQUES_CODE = 7;
    private GridView gridview;
    private LayoutInflater inflater;

    private int dialogPosition;
    private int columnWidth = 300;
    private String[] ean = {"", "", "", "", "", ""};
    private String[] PLUurls = {"", "", "", "", "", ""};
    private Context _context;
    private ImageAdapter adapter;

    public ButtonsListFragmentTest() {
    }

    private static void callOnActivityResultOnChildFragments(Fragment parent, int requestCode, int resultCode, Intent data) {
        FragmentManager childFragmentManager = parent.getChildFragmentManager();
        List<Fragment> childFragments = childFragmentManager.getFragments();
        for (Fragment child : childFragments) {
            if (child != null && !child.isDetached() && !child.isRemoving()) {
                child.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_buttons_list, container, false);
        gridview = v.findViewById(R.id.gridview);
        createGridView();
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _context = context;
    }

    void createGridView() {

        adapter = new ImageAdapter(_context);
        gridview.setAdapter(adapter);
        gridview.setColumnWidth(columnWidth);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!ean[position].equals("")) {
                    ToastUtils.warning("Must implement onPLUClick(ean[position]) method !");//myActivty.onPLUClick(ean[position]);
                }
            }
        });

        gridview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {
                dialogPosition = position;

                AlertDialog.Builder alert = new AlertDialog.Builder(_context);
                alert.setMessage("Velg varenummer og bildelink URL"); //"Choose itemnumber and picture URL"
                alert.setView(getLayoutInflater().inflate(R.layout.change_plu, null)); //<-- layout containing EditText
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        String value = ((EditText) ((AlertDialog) dialog).findViewById(R.id.D10itemNo)).getText().toString();
                        ean[dialogPosition] = value; //save input string
                        value = ((EditText) ((AlertDialog) dialog).findViewById(R.id.D10url)).getText().toString();
                        PLUurls[dialogPosition] = value; //save input string
                        dialog.dismiss();
                        adapter.notifyDataSetChanged();
                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Do nothing.
                        dialog.dismiss();
                    }
                });
                /*
                Change 'ean[]' value from dialog editText, and pick photo. Result returned to onActivityResult()....
                 */
                alert.setNeutralButton("Get Image", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String value = ((EditText) ((AlertDialog) dialog).findViewById(R.id.D10itemNo)).getText().toString();
                        ean[dialogPosition] = value; //save input string
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        getActivityStarterFragment().startActivityForResult(pickPhoto, FILE_PICKER_REQUES_CODE);
                    }
                });

                alert.setTitle("PLU nr " + position);
                AlertDialog dia = alert.create();

                dia.show();

                if (!ean[dialogPosition].equals(""))
                    ((EditText) dia.findViewById(R.id.D10itemNo)).setText(ean[dialogPosition]); //display current value...;
                if (!PLUurls[dialogPosition].equals(""))
                    ((EditText) dia.findViewById(R.id.D10url)).setText(PLUurls[position]);

                return true;
            }
        });
    }

    private Fragment getActivityStarterFragment() {
        if (getParentFragment() != null) {
            return getParentFragment();
        }
        return this;
    }

    /*
    Call back from pick photo dialog.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILE_PICKER_REQUES_CODE) {
            if (resultCode == RESULT_OK) {
                Uri selectedImage = data.getData();
                PLUurls[dialogPosition] = String.valueOf(selectedImage);
                adapter.notifyDataSetChanged();
            }
        }
        callOnActivityResultOnChildFragments(this, requestCode, resultCode, data);
    }

    class ImageAdapter extends BaseAdapter {

        public ImageAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return PLUurls.length;
        }

        @Override
        public Object getItem(int position) {
            return PLUurls[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        // create a new ImageView for each item referenced by the Adapter
        @SuppressLint("ResourceAsColor")
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView;
            TextView textView;
            Drawable imageBitmap;

            if (convertView == null) {
                // if it's not recycled, initialize some attributes
                convertView = inflater.inflate(R.layout.button_layout, parent, false);
            }

            imageView = convertView.findViewById(R.id.btnImage);
            textView = convertView.findViewById(R.id.tvItemNo);
            /*
             set textView and make imageView from local arrays 'PLUurls[]', 'ean[]'.
             PLUurls[] - Strin array with path file location (remote or local  content:/)
             */
            textView.setText(ean[position]);

            if (PLUurls[position] != null && !PLUurls[position].equals("")) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    if (!PLUurls[position].contains("http")) {
                        imageView.setImageURI(null);
                        imageView.setImageURI(Uri.parse(PLUurls[position]));
                    } else {
                        //TODO Need implement DoUrlBitmapLoad class
//                        DoUrlBitmapLoad bmLoad = new DoUrlBitmapLoad();
//                        bmLoad.setView(imageView);
//                        try {
//                            bmLoad.execute(new URL(PLUurls[position]));
//                        } catch (MalformedURLException e) {
//                            e.printStackTrace();
//                        }
                    }
                }
            }
            return convertView;
        }
    }
}
