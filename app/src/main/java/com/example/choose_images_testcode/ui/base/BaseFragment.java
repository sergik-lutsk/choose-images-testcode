package com.example.choose_images_testcode.ui.base;

import android.content.Context;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.choose_images_testcode.ui.utils.AnimationViewUtils;
import com.example.choose_images_testcode.ui.utils.fragments.FragmentChangeUtils;
import com.example.choose_images_testcode.ui.utils.fragments.FragmentsAnimationId;

public class BaseFragment extends Fragment {

    public void showView(Context context, View view) {
        AnimationViewUtils.showView(context, view);
    }

    public void hideView(Context context, View view) {
        AnimationViewUtils.hideView(context, view);
    }

    public void changeViews(Context context, View newView, View oldView) {
        AnimationViewUtils.changeViews(newView, oldView, context);
    }

    public void setFragment(FragmentManager fragmentManager, Fragment fragment, int layoutResIs) {
        FragmentChangeUtils.setFragment(fragmentManager, fragment, layoutResIs);
    }

    public void changeFragment(FragmentManager fragmentManager, Fragment fragment, int layoutResIs) {
        FragmentChangeUtils.changeFragment(fragmentManager, fragment, layoutResIs);
    }

    public void changeFragmentWithAnimation(FragmentManager fragmentManager, Fragment fragment, int layoutResIs, FragmentsAnimationId fragmentsAnimationId) {
        FragmentChangeUtils.changeFragmentWithAnimation(fragmentManager, fragment, layoutResIs, fragmentsAnimationId);
    }
}
